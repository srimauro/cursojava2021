package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicio15 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

       Scanner sc = new Scanner(System.in);
		
	    System.out.println("Ingrese la letra");
	    char letra = sc.next().charAt(0);
	    String caractAuto = null;
	    
	    
	    System.out.println("\nCaracterísticas del Auto:");
	    System.out.println("-------------------------");
	    
	    System.out.println();
	    
	    switch(letra)
	      {
		  
		   case 'a':
			   caractAuto = "4 ruedas y un motor";
			   break;
	  	   case 'b':
	  		 caractAuto  = "4 ruedas, un motor, cierre centralizado y aire";
		       break;
	  	   case 'c':
	  		 caractAuto  = "4 ruedas, un motor, cierre centralizado, aire y airbag";
			   break;
		   default:
			   caractAuto  = "Categoría inexistente";
	  	  	}   
		  		   
	      System.out.println(caractAuto );

   
	}
	}


