package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicio8 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner sc = new Scanner(System.in);
		 int piedra = 0;
		 int papel = 1;
		 int tijera = 2;
		// int comp1;
		// int comp2;
		 
		    System.out.println("Ingrese 0 para piedra, 1 para papel o 2 para tijera");
		    System.out.println("\nCompetidor 1, ingrese el valor");
		    int comp1 = sc.nextInt();
		    
		    System.out.println("\nCompetidor 2, ingrese el valor");
		    int comp2 = sc.nextInt();
		    
		    
		    if ((comp1 == 0 && comp2 == 0) || (comp1 == 1 && comp2 == 1) || (comp1 == 2 && comp2 == 2))
		    	System.out.println("Empate"); 
		   else if (comp1 == 0 && comp2 == 1)
			   System.out.println("Gano Competidor 2");   
		   else if (comp1 == 0 && comp2 == 2)
               System.out.println("Gano Competidor 1");
		   else if (comp1 == 1 && comp2 == 0)
               System.out.println("Gano Competidor 1");
		   else if (comp1 == 1 && comp2 == 2)
               System.out.println("Gano Competidor 2"); 
		   else if (comp1 == 2 && comp2 == 0)
               System.out.println("Gano Competidor 2"); 
		   else if (comp1 == 2 && comp2 == 1)
               System.out.println("Gano Competidor 1"); 
		  
	}

}
