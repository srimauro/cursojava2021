package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicio1 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        
    Scanner sc = new Scanner(System.in);
    System.out.println("Ingrese nota 1");
    float nota1 = sc.nextFloat();
    
    System.out.println("Ingrese nota 2");
    float nota2 = sc.nextFloat();
    
    System.out.println("Ingrese nota 3");
    float nota3 = sc.nextFloat();
    
    float promedio = (nota1+nota2+nota3)/3;
    
    if (promedio >= 7) 
    	 System.out.println("Aprobaste con un promedio de: "+promedio);
    else
    	System.out.println("Reprobaste con un promedio de: "+promedio);
    
	}

}
