package modulo3;

import java.util.Scanner;

public class Mod3_Ejercicio12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
	    System.out.println("Ingrese un n�mero");
	    int nro = sc.nextInt();
	    
	    if (nro >=1 && nro <= 12)
	     
	    	 System.out.println("El n�mero "+ nro + " a la primer docena");
	    else if (nro >=13 && nro <= 24)
	    	System.out.println("El n�mero "+ nro + " a la segunda docena");
	    else if (nro >=25 && nro <= 36)
	    	System.out.println("El n�mero "+ nro + " a la tercer docena");
	    else
	    	System.out.println("El n�mero "+ nro + " est� fuera de rango");
	}

}
