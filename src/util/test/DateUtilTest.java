package util.test;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.DateUtil;

public class DateUtilTest {
	//lote de pruebas
		Date fechaCumple;
		Date diaSemana;
		Date fTest;
		String sFormatoFecha ;
		String sFecha;
	@Before
	public void setUp() throws Exception {
		//se ejecuta antes de cada prueba
			Calendar cal = Calendar.getInstance();
			cal.set(1970, Calendar.DECEMBER, 3);
			fechaCumple = cal.getTime();
		
			diaSemana = cal.getTime();
			
			sFormatoFecha = "dd/mm/yyyy";
		//	sFormatoFecha = "EEEEEEEEE dd-MM-yyyy- HH:mm:ss";
			sFecha= "17/10/2021";	
		//	cal.set(1970, Calendar.DECEMBER, 3);
			fTest = cal.getTime();
		}
	

	@After
	public void tearDown() throws Exception {
		//se ejecuta despes de cada prueba
				fechaCumple = null;
	}

	@Test
	public void testGetAnio() {
		assertEquals(1970, DateUtil.getAnio(fechaCumple));
	}

	@Test
	public void testGetMes() {
		assertEquals(12, DateUtil.getMes(fechaCumple));
	}
	
	@Test
	public void testGetDia() {
		assertEquals(3, DateUtil.getDia(fechaCumple));
	}
	
	@Test
	public void isFinDeSemana() {
		assertEquals(true, DateUtil.isFinDeSemana(fechaCumple));
	}
	
	@Test
	public void isDiaDeSemana() {
		assertEquals(true, DateUtil.isDiaDeSemana(fechaCumple));
	}
	
	@Test // 5 indica que es el dia 5 de la semana
	public void getDiaDeSemana() {
		assertEquals(5, DateUtil.getDiaDeSemana(fechaCumple));
	}
	
	@Test
	public void asDate(){
		assertEquals(fTest, DateUtil.asDate(sFormatoFecha,sFecha));
		
	}
}


