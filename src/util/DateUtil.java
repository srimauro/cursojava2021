package util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;

/**
 * Esta clase tiene una serie de metodos vinculados con el manejo de fechas ofreciendo un conjuto de servicios
 * @author 
 *
 */

public class DateUtil {
	
	/**
	 * Este metodo devuelve el a�o de la fecha que se envia por parametro
	 * @param pFecha correspone al parametro donde se envia la fecha
	 * @return devuele un valor de tipo int donde se encuentra el 
	 */
	public static int getAnio(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);		
		return cal.get(Calendar.YEAR);
	}
	
	public static int getMes(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);		
		return cal.get(Calendar.MONTH)+1;
	}

	public static int getDia(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);		
		return cal.get(Calendar.DATE);
	}
	
	public static boolean isFinDeSemana(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);		
		if (cal.get(Calendar.DAY_OF_WEEK) == 6 || cal.get(Calendar.DAY_OF_WEEK) == 7 )
		return true;
	//	return false;
		else return false;
	}
		
		public static boolean isDiaDeSemana(Date pFecha){
			Calendar cal = Calendar.getInstance();
			cal.setTime(pFecha);		
			if (cal.get(Calendar.DAY_OF_WEEK) != 6 && cal.get(Calendar.DAY_OF_WEEK) != 7 )
			return true;
		//	return false;
			else return false;
			
			//
		
	}
		
		public static int getDiaDeSemana(Date pFecha){
		Calendar cal = Calendar.getInstance();
		cal.setTime(pFecha);		
		return cal.get(Calendar.DAY_OF_WEEK);
		}
		
		
		public static Date asDate(String sFormatoFecha, String sFecha){
				
			
			SimpleDateFormat formatoDelTexto = new SimpleDateFormat(sFormatoFecha);
			//String strFecha = �2007-12-25�;
			Date fecha = null;
			try {

			fecha = formatoDelTexto.parse(sFecha);

			} catch (ParseException ex) {

			ex.printStackTrace();

			}

		//	System.out.println(fecha.toString());
			
				
			
			return (fecha);
			
			}
}
