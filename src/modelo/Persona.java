package modelo;

public class Persona {

	
		//los atributos
	
		private String apellido;
		private String nombre;
		
		//constructores
		
		public Persona() {	}
		
		public Persona(String pNombre, String pApellido){
			nombre = pNombre;
			apellido = pApellido;		
		}
		//accessors
		public void setNombre(String pNombre){
			nombre=pNombre;
		}
		public String getNombre(){
			return nombre;
		}
		
		public void setApellido(String pApellido){
			nombre=pApellido;
		}
		public String getApellido(){
			return apellido;
		}

			
		//metodos de negocio
//		public void acreditar(float pMonto){
//			saldo +=pMonto;
//		}
		
	//	public abstract void debitar(float pMonto);
		
		public boolean equals(Object obj){
		
			boolean bln =false;
			if(obj instanceof Persona){
				//downCast
			Persona per =(Persona) obj;
				
				bln = per.getNombre().equals(nombre) &&
					  per.getApellido().equals(apellido);			
			}
			return bln;
		}
		
//		public int hashCode(){		
//			return numero + (int) saldo;
//		}
		
		
		
		
		public String toString(){
			StringBuilder sb = new StringBuilder("\nNombre=");
			sb.append(nombre);
			sb.append(", Apellido=");
			sb.append(apellido);		
			return sb.toString();
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((apellido == null) ? 0 : apellido.hashCode());
			result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
			return result;
		}
		
//		public int hashCode() {
//			return this.hashCode();
//		}
		
//		@Override
//		public boolean equals(Object obj) {
//			if (this == obj)
//				return true;
//			if (obj == null)
//				return false;
//			if (getClass() != obj.getClass())
//				return false;
//			Persona other = (Persona) obj;
//			if (apellido == null) {
//				if (other.apellido != null)
//					return false;
//			} else if (!apellido.equals(other.apellido))
//				return false;
//			if (nombre == null) {
//				if (other.nombre != null)
//					return false;
//			} else if (!nombre.equals(other.nombre))
//				return false;
//			return true;
//		}
		
		
	

	}


