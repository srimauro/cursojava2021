package modelo;

public class Profesor extends Persona{
  private String iosfa;

public String getIosfa() {
	return iosfa;
}

public void setIosfa(String iosfa) {
	this.iosfa = iosfa;
}
  

public Profesor() {
	//llamar al constructor del padre.
	super();		
}

public Profesor(String pNom, String pApe, String pIosfa) {
	super(pNom, pApe);
	iosfa = pIosfa;		
}

public boolean equals(Object obj){
	
	boolean bln=false;
	if(obj instanceof Profesor){
		//estoy haciendo un downCast
		Profesor pr = (Profesor) obj;
		bln=  super.equals(obj) 			&&
			  pr.getIosfa() ==iosfa;
	}
	return bln;
}

public int hashCode(){
	return super.hashCode() + iosfa.hashCode();
}

public String toString(){
	StringBuilder sb = new StringBuilder(super.toString());
	sb.append(", Iosfa=");
	sb.append(iosfa);
	
	return sb.toString();
}
}
