package modelo;



public class Alumno extends Persona{
   private int legajo;

public int getLegajo() {
	return legajo;
}

public void setLegajo(int legajo) {
	this.legajo = legajo;
}
   
public Alumno() {
	//llamar al constructor del padre.
	super();		
}

public Alumno(String pNom, String pApe, int pLegajo) {
	super(pNom, pApe);
	legajo = pLegajo;		
}


public boolean equals(Object obj){
	
	boolean bln=false;
	if(obj instanceof Alumno){
		//estoy haciendo un downCast
		Alumno al = (Alumno) obj;
		bln=  super.equals(obj) 			&&
			  al.getLegajo() ==legajo;
	}
	return bln;
}

public int hashCode(){
	return super.hashCode() + (int) legajo;
}

public String toString(){
	StringBuilder sb = new StringBuilder(super.toString());
	sb.append(", Legajo=");
	sb.append(legajo);
	
	return sb.toString();
}
}

