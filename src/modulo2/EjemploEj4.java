package modulo2;

public class EjemploEj4 {

	public static void main(String[] args) {
		byte b =(byte)( Math.random()*Byte.MAX_VALUE);
		short s =(short)( Math.random()*Short.MAX_VALUE);
		int i =(int)( Math.random()*Integer.MAX_VALUE);
		long l =(long)( Math.random()*Long.MAX_VALUE);
		
		int sumabb = b + b;
		int sumabs = b + s;
		int sumabi = b + i;
		int sumaii = i + i;
		long sumasl = s + l;
		
		System.out.println("b=" + b);
		System.out.println("s=" + s);
		System.out.println("i=" + i);
		System.out.println("l=" + l);
		System.out.println("\nsumabb=" + sumabb);
		System.out.println("sumabs=" + sumabs);
		System.out.println("sumabi=" + sumabi);
		System.out.println("sumaii=" + sumaii);
		System.out.println("sumasl=" + sumasl);

	}

}