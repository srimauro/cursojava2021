package modulo2;

public class EjemploEj1 {

	public static void main(String[] args) {
		int i =0;
		int j=++i;
		
		System.out.println("j=" + j);
		System.out.println("i=" + i);
		System.out.println("ahora si los quiero ver");
		//hasta aca i =1;
		
		System.out.println(i++ + "-" + ++i);
		//a) 2 - 3
		//b) 1 - 2
		//c) 1 - 3
		//d) ninguna de las anteriores
		
		///desplazamientos
		i = 1357;
		j = i>>5;
		System.out.println("el valor de " + i + " desplazada 5 lugares es " + j);
		int x = -1357;
		int y = x >> 5;
		System.out.println("el valor de " + x + "desplazados 5 lugares es " + y);

		//tipos de datos
		// byte, short, int, log, float double
		// -> auto cast
		// <- necesito casteo
		byte b=34;
		short s =b;
		 i =1357;
		 b = (byte)i; //vos te haces responsable
		 s = (short)i;
		 System.out.println("i=" + i);
		 System.out.println("b=" + b );
		 System.out.println("s=" + s );
		 
		 int imin = Integer.MIN_VALUE;
		 System.out.println("\n\n\nEl minimo entero es " + imin);
		 System.out.println("\n\n\nEl Maximo abs es " + (Math.abs(imin)-1));
		 
		
	}

}

