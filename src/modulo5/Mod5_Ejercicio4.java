package modulo5;

import java.util.Scanner;

public class Mod5_Ejercicio4 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
			
	String strTextoOriginal = "esto es una prueba de la clase String";
		
	   int vocales = 0;
	   int consonantes = 0;
	   
		
		//voy a recorer el string
		for (int i =0;i<strTextoOriginal.length();i++)
			if ((strTextoOriginal.charAt(i)=='a') || (strTextoOriginal.charAt(i)=='e') || (strTextoOriginal.charAt(i)=='i') || (strTextoOriginal.charAt(i)=='o') || (strTextoOriginal.charAt(i)=='u')) 
			    vocales++;
			else
				consonantes++;
		
			System.out.println("Cantidad de vocales = " + vocales + " Cantidad de consonantes = " + consonantes);

	}

}
